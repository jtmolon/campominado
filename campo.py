# -*- coding: utf-8 -*-

VAZIO = 0
BOMBA = 9
BANDEIRA = 100

import random

class Casa(object):
    
    def __init__(self, numero=0, visivel=False):
        """
        """
        self.numero = numero
        self.visivel = visivel

    def show(self):
        return str(self.numero) if self.visivel else "X"

class CampoMinado(object):
    
    def __init__(self, dimensao = 10, bombas = 10):
        """
        """
        self.dimensao = dimensao
        self.bombas = bombas
        self.campo = []
        for i in range(0,self.dimensao):
            self.campo.append([])
            linha = self.campo[i]
            for j in range(0,self.dimensao):
                linha.append(Casa(0))
        self.distribuirBombas()
        self.setarNumeros()

    def distribuirBombas(self):
        """
        """
        indices = range(0,self.dimensao)
        dio = []
        for i in range(0,self.bombas):
            linha = random.choice(indices)
            coluna = random.choice(indices)
            while (linha, coluna) in dio:
                linha = random.choice(indices)
                coluna = random.choice(indices)
            dio.append((linha, coluna))
            self.campo[linha][coluna].numero = BOMBA
        return False

    def setarNumeros(self):
        """
        """
        for i in range(0,self.dimensao):
            for j in range(0,self.dimensao):
                if not self.campo[i][j].numero:
                    bombas = 0
                    colunaAntes = j - 1 >= 0
                    colunaDepois = j + 1 <= self.dimensao - 1
                    linhaAntes = i - 1 >= 0
                    linhaDepois = i + 1 <= self.dimensao - 1
                    if colunaAntes and self.campo[i][j - 1].numero == BOMBA:
                        bombas += 1
                    if colunaDepois and self.campo[i][j + 1].numero == BOMBA:
                        bombas += 1
                    if linhaAntes:
                        if colunaAntes and self.campo[i - 1][j - 1].numero == BOMBA:
                            bombas += 1
                        if self.campo[i - 1][j].numero == BOMBA:
                            bombas += 1
                        if colunaDepois and self.campo[i - 1][j + 1].numero == BOMBA:
                            bombas += 1
                    if linhaDepois:
                        if colunaAntes and self.campo[i + 1][j - 1].numero == BOMBA:
                            bombas += 1
                        if self.campo[i + 1][j].numero == BOMBA:
                            bombas += 1
                        if colunaDepois and self.campo[i + 1][j + 1].numero == BOMBA:
                            bombas += 1
                    self.campo[i][j].numero = bombas
                        
        return False

    def abrePosicao(self, linha, coluna):
        colunaAntes = coluna - 1 >= 0
        colunaDepois = coluna + 1 <= self.dimensao - 1
        linhaAntes = linha - 1 >= 0
        linhaDepois = linha + 1 <= self.dimensao - 1
        if colunaAntes and self.campo[linha][coluna - 1].numero != BOMBA and not self.campo[linha][coluna - 1].visivel:
            self.campo[linha][coluna - 1].visivel = True
            if not self.campo[linha][coluna - 1].numero:
                self.abrePosicao(linha, coluna - 1)
        if colunaDepois and self.campo[linha][coluna + 1].numero != BOMBA and not self.campo[linha][coluna + 1].visivel:
            self.campo[linha][coluna + 1].visivel = True
            if not self.campo[linha][coluna + 1].numero:
                self.abrePosicao(linha, coluna + 1)
        
        if linhaAntes:
            if colunaAntes and self.campo[linha - 1][coluna - 1].numero != BOMBA and not self.campo[linha - 1][coluna - 1].visivel:
                self.campo[linha - 1][coluna - 1].visivel = True
                if not self.campo[linha - 1][coluna - 1].numero:
                    self.abrePosicao(linha - 1, coluna - 1)
            if self.campo[linha - 1][coluna].numero != BOMBA and not self.campo[linha - 1][coluna].visivel:
                self.campo[linha - 1][coluna].visivel = True
                if not self.campo[linha - 1][coluna].numero:
                    self.abrePosicao(linha - 1, coluna)
            if colunaDepois and self.campo[linha - 1][coluna + 1].numero != BOMBA and self.campo[linha - 1][coluna + 1].visivel:
                self.campo[linha - 1][coluna + 1].visivel = True
                if not self.campo[linha - 1][coluna + 1].numero:
                    self.abrePosicao(linha - 1, coluna + 1)
        if linhaDepois:
            if colunaAntes and self.campo[linha + 1][coluna - 1].numero != BOMBA and not self.campo[linha + 1][coluna - 1].visivel:
                self.campo[linha + 1][coluna - 1].visivel = True
                if not self.campo[linha + 1][coluna - 1].numero:
                    self.abrePosicao(linha + 1, coluna - 1)
            if self.campo[linha + 1][coluna].numero != BOMBA and not self.campo[linha + 1][coluna].visivel:
                self.campo[linha + 1][coluna].visivel = True
                if not self.campo[linha + 1][coluna].numero:
                    self.abrePosicao(linha + 1, coluna)
            if colunaDepois and self.campo[linha + 1][coluna + 1].numero != BOMBA and self.campo[linha + 1][coluna + 1].visivel:
                self.campo[linha + 1][coluna + 1].visivel = True
                if not self.campo[linha + 1][coluna + 1].numero:
                    self.abrePosicao(linha + 1, coluna + 1)
            

    def jogada(self, linha, coluna):
        casa = self.campo[linha][coluna]
        if not casa.visivel:
            casa.visivel = True
            if not casa.numero: #se for 0 abre os vizinhos
                self.abrePosicao(linha, coluna)
            if casa.numero == BOMBA:
                print "PERDEU"
        return False


campo_minado = CampoMinado()
while True:
    for dio in campo_minado.campo:
        print [xita.show() for xita in dio]
    print ""
    for dio in campo_minado.campo:
        print [xita.numero for xita in dio]
#    break
    linha = raw_input("Informe a linha: ")
    coluna = raw_input("Informe a coluna: ")
    linha = int(linha)
    coluna = int(coluna)
    campo_minado.jogada(linha, coluna)
